##programname: classify4me [formula]
#!/usr/bin/env python3
# -*- coding:utf-8 -*-

__copyright__ = "Copyright 2017, The NOMAD Project"
__author__ = "Daria M. Tomecka and Fawzi Mohamed"
__email__ = "tomecka@fhi-berlin.mpg.de"
#created:
#__date__ = "29/11/16"
#normalized:
__date__ = "05/04/17"

import setup_paths
from nomadcore.parser_backend import JsonParseEventsWriterBackend
from nomadcore.parse_streamed_dicts import ParseStreamedDicts
import json
import sqlite3
import sys, time, os.path, re
from io import open
from nomadcore.local_meta_info import loadJsonFile, InfoKindEl
import logging

DB_FILE = os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "SM_all08.db"))
#OUTPUT_FILE = sys.argv[2]    


#DB_FILE = "test" + str(time.time()) + ".db"
#loaded_json = json.load(open(DB_FILE, encoding="utf-8"))

# Connecting to the database file
DB = os.path.exists(DB_FILE)
conn = sqlite3.connect(DB_FILE)
cur = conn.cursor()

def formula2dict(formula):
    atomRe = re.compile(r"(?P<symbol>[A-Z][a-z]*)(?P<count>[0-9]*)")
    fDict = {}
    i = 0
    for m in atomRe.finditer(formula):
        if i != m.start():
            logging.warn("skipping %r when parsing %s", INPUT_FORMULA[i:m.start], INPUT_FORMULA)
        i = m.end()
        at = m.group("symbol")
        count = m.group("count")
        if not count:
            count = 1
        else:
            count = int(count)
        fDict[at] = fDict.get(at,0) + count
    return fDict

def dict2formula(fDict):
    keys = sorted(fDict.keys())
    res = ""
    for k in keys:
        count = fDict[k]
        if count:
            res += k
            if count != 1:
                res += str(count)
    return res

def to100(fDict):
    tCount = sum(fDict.values())
    res = {}
    if tCount:
        for k, v in fDict.items():
            res[k] = (100*v+tCount -1)//tCount
    return res

def classify4me(INPUT_FORMULA, backend):
    
    normalizedF = dict2formula(to100(formula2dict(INPUT_FORMULA)))
    # logging.warn('%r -> %r starting Springer Materials normalization', INPUT_FORMULA, normalizedF)
    res={}
    
    cur.execute("""
    select entry.space_group_number, entry.entry_id, entry.alphabetic_formula from entry 
    where entry.normalized_formula = ( %r ) group by entry.space_group_number, entry.entry_id;"""  % normalizedF)


    #build dictionary 
    #print(cur.fetchone()[0])

    results = cur.fetchall()
    for group , sprId, formula in results:
        k = (group,formula)
        if not k in res:
            res[k]={}
        if not "section_springer_id" in res[k]:
            res[k]["section_springer_id"]=[]
        spRefs = res[k]["section_springer_id"]
        if sprId.startswith("sd_"):
            url="http://materials.springer.com/isp/crystallographic/docs/" + sprId
        else:
            raise Exception("does not know how to construct url for id " + sprId)
        spRefs.append({
            "springer_id": sprId,
            "springer_url": url
        })
###    fOut.write('space group: \n %s \n'% results)

        #print('space group:',results, '\n')


    cur.execute("""
    select entry.space_group_number, entry.alphabetic_formula, compound_classes.compound_class_name, count(*)
    from
        entry
        join entry_compound_class on entry.entry_nr = entry_compound_class.entry_nr
        join compound_classes on compound_classes.compound_class_nr = entry_compound_class.compound_class_nr
        where
           entry.normalized_formula = ( %r )
        group by entry.space_group_number, compound_classes.compound_class_name
    ;""" % normalizedF)


    results = cur.fetchall()
    #print('compound class:',results, '\n')
    for group , formula, sprCC, count in results:
        k = (group, formula)
        if not k in res:
            res[k]={}
        if not "section_springer_compound_class" in res[k]:
            res[k]["section_springer_compound_class"] = []
        spRefs = res[k]["section_springer_compound_class"]
        spRefs.append({
            "springer_compound_class": sprCC,
            "springer_number_of_compound_class_reference_per_material": count
        })
###    fOut.write('compound class: \n % r  \n' % results)


    cur.execute("""
    select entry.space_group_number, entry.alphabetic_formula, classification.classification_name, count(*)
    from
        entry
        join entry_classification on entry.entry_nr = entry_classification.entry_nr
        join classification on classification.classification_nr = entry_classification.classification_nr
        where
            entry.normalized_formula = ( %r )
        group by entry.space_group_number, classification.classification_name
    ;""" % normalizedF)

    results = cur.fetchall()
    #print('classification:', results, '\n')
    for group , formula, sprC, count in results:
        k = (group, formula)
        if not k in res:
            res[k]={}
        if not "section_springer_classification" in res[k]:
            res[k]["section_springer_classification"] = []
        spRefs = res[k]["section_springer_classification"]
        spRefs.append({
            "springer_classification": sprC,
            "springer_number_of_classification_reference_per_material": count
        })
                                            
###    fOut.write('classification:\n %r \n' % results)

    cur.execute("""
    select entry.space_group_number, entry.alphabetic_formula, reference.reference_name, entry.entry_id
    from
        entry
        join entry_reference on entry.entry_nr = entry_reference.entry_nr
        join reference on reference.reference_nr = entry_reference.reference_nr
        where
            entry.normalized_formula = ( %r )
        group by entry.space_group_number, reference.reference_name
    ;""" % normalizedF)

    results = cur.fetchall()
    #print('references:',results)
    ###
###    fOut.write('references: \n %r \n' % results)
#to be corrected
    for group , formula, sprRef, entryId in results:
        k = (group, formula)
        if not k in res:
            res[k]={}
        if not "section_springer_id" in res[k]:
            res[k]["section_springer_id"]=[]
        spRefs = res[k]["section_springer_id"]

        
        if entryId.startswith("sd_"):
            url="http://materials.springer.com/isp/crystallographic/docs/" + entryId
        else:
            raise Exception("does not know how to construct url for id " + entryId)
        #print("TEST:", entryId)
        #print("TEST2:", sprId)
        found = False
        for spId in spRefs:
            if spId.get("springer_id") == entryId:
                spId["section_springer_references"] = spId.get("section_springer_references",[]) + [{
                    "springer_reference": sprRef
                }]
                found = True
        if not found:
            spRefs.append({
                "springer_id": entryId,
                "springer_url": url,
                "section_springer_references": [{
                    "springer_reference": sprRef
                }]
            })
                                            
    #print('}')
    for (sp, f),entry in res.items():
        try:
            spNr = int(sp)
        except:
            spNr = -1
        entry["springer_space_group_number"] = spNr
        entry["springer_formula"] = f

    #json.dump(results, fOut, sort_keys=False, ensure_ascii=False, indent=2)
###    fOut.write("} \n")

### NORMALIZATION ###

    #For normalization replaced json.dump with loop below
    ##json.dump({"section_springer_material":list(res.values())}, sys.stdout, indent=2, sort_keys=True, ensure_ascii=False)
    for mat in res.values():
        matS=backend.openSection("section_springer_material")

        for cl in mat.get("section_springer_classification",[]):
            clS = backend.openSection("section_springer_classification")
            if "springer_classification" in cl:
                backend.addValue("springer_classification",cl["springer_classification"])
            if "springer_number_of_classification_reference_per_material" in cl:
                backend.addValue("springer_number_of_classification_reference_per_material",cl["springer_number_of_classification_reference_per_material"])
            backend.closeSection("section_springer_classification",clS)

        for cl in mat.get("section_springer_compound_class",[]):
            clS = backend.openSection("section_springer_compound_class")
            if "springer_compound_class" in cl:
                backend.addValue("springer_compound_class",cl["springer_compound_class"])
            if "springer_number_of_compound_class_reference_per_material" in cl:
                backend.addValue("springer_number_of_compound_class_reference_per_material",cl["springer_number_of_compound_class_reference_per_material"])
            backend.closeSection("section_springer_compound_class",clS)

        for cl in mat.get("section_springer_id",[]):
            clS = backend.openSection("section_springer_id")
            if "springer_id" in cl:
                backend.addValue("springer_id",cl["springer_id"])
            if "springer_url" in cl:
                backend.addValue("springer_url",cl["springer_url"])
            backend.closeSection("section_springer_id",clS)

            for ref in cl.get("section_springer_references",[]):
                refS = backend.openSection("section_springer_references")
                if "springer_reference" in ref:
                    backend.addValue("springer_reference",ref["springer_reference"])             
                backend.closeSection("section_springer_references",refS)


        if "springer_formula" in mat:
            backend.addValue("springer_formula",mat["springer_formula"])
        
        if "springer_space_group_number" in mat:
            backend.addValue("springer_space_group_number",mat["springer_space_group_number"])
            
            
        backend.closeSection("section_springer_material",matS)

def main():
    metapath = '../../../../nomad-meta-info/meta_info/nomad_meta_info/' +\
          'common.nomadmetainfo.json'
    metaInfoPath = os.path.normpath(
    os.path.join(os.path.dirname(os.path.abspath(__file__)), metapath))
  
    metaInfoEnv, warns = loadJsonFile(filePath=metaInfoPath,
                                      dependencyLoader=None,
                                      extraArgsHandling=InfoKindEl.ADD_EXTRA_ARGS,
                                      uri=None)
    fOut = sys.stdout
    backend = JsonParseEventsWriterBackend(metaInfoEnv, fOut)

    #Start 
    calcContext=sys.argv[1]+"/_"
    backend.startedParsingSession(
        calcContext,
        parserInfo = {'name':'SpringerNormalizer', 'version': '1.0'})
    res = "ParseSuccess"

    dictReader = ParseStreamedDicts(sys.stdin)
    while True:
        springerInfo = dictReader.readNextDict()
        if springerInfo is None:
            break
        calcContext = springerInfo['calculation_uri']
        knownFormulas = set()
        for sysInfo in springerInfo["system_info"]:
            try:
                formula = sysInfo.get("system_composition")
                if formula and formula not in knownFormulas:
                    knownFormulas.add(formula)
                    backend.openContext(calcContext)
                    classify4me(formula, backend)
                    backend.closeContext(calcContext)
                    fOut.flush()
            except:
                res = "ParseFailure"
                logging.exception("exception trying to calculate springer data for %s", sysInfo)
    backend.finishedParsingSession(res, None)
    fOut.flush()
    ### 
    #  con.create_function("", 1, )
    cur.close()
    conn.close()

if __name__ == '__main__':
    main()

