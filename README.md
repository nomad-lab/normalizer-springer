
Adds data from the springer materials database to the calculation

This normalizer adds the info concerning Springer classifications.

It is part of the NOMAD Laboratory.

The original repository lives at:
https://gitlab.mpcdf.mpg.de/nomad-lab/normalizer-springer

More info on springer classification can be found here:

https://gitlab.mpcdf.mpg.de/nomad-lab/normalizer-springer/wikis/info-on-springer-classification

Info on the normalization:

https://gitlab.mpcdf.mpg.de/nomad-lab/normalizer-springer/wikis/normalization-of-springer-classification